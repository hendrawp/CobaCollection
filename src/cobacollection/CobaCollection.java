package cobacollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CobaCollection {

    public static void main(String[] args) {
        
        ArrayList list = new ArrayList();
        list.add("sdasdas");
        list.add(new String("sdasdasdas"));
        list.add(5);
        list.add(5.5);
        
        System.out.println(list.get(0));
        for(Object x : list)
            System.out.println(x);
        
        ArrayList<String> listString = new ArrayList<>();
        
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Andi", 60);
        map.put("Bagus", 70);
        map.put("Rudi", 50);
        
        System.out.println(map.get("Bagus"));
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key+"|"+value);
        } 
        
    }
    
}
